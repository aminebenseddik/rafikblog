import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../services/articles.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Article } from '../models/article';

@Component({
    templateUrl: './app/pages/article-detail-page.component.html',
    providers: [ ArticleService ]
})
export class ArticleDetailPageComponent implements OnInit {
  article: Article;
  // insecureHtml: SafeHtml;
  insecureHtml: string;

  constructor(
    private articleService: ArticleService,
    private domSanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute
    ) {
    this.article = new Article();
  }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params['id'];

    this.articleService.getArticle(id).subscribe((article) => {
      console.log(article);
      this.article = article;
    });

    // this.insecureHtml =  this.domSanitizer.bypassSecurityTrustHtml('<p><b>Paragraphe</b></p>');
    this.insecureHtml = '<p><b>Paragraphe</b></p>';
  }
}
