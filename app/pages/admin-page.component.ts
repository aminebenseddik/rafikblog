import { Component, OnInit } from '@angular/core';
import { Article } from '../models/article';
import { ArticleService } from '../services/articles.service';

@Component({
    templateUrl: './app/pages/admin-page.component.html',
    providers: [ ArticleService ]
})
export class AdminPageComponent implements OnInit {
  articles: Article[];

  constructor(private articleService: ArticleService) {}

  ngOnInit() {
    this.articleService.getArticles().subscribe((articles) => {
      this.articles = articles;
      console.log(articles);
    });
  }

}
