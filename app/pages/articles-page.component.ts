import { Component, OnInit } from '@angular/core';
import { Article } from '../models/article';
import { ArticleService } from '../services/articles.service'; 


@Component({
    templateUrl: './app/pages/artilces-page.component.html',
    providers: [ ArticleService ]
})
export class ArticlesPageComponent implements OnInit {
  articles: Article[];

  constructor(private articleService: ArticleService) {
    console.log(this.articleService.getArticles());
  }

  ngOnInit() {
    // this.articles = this.articleService.getArticles();
    /*this.articleService.getArticles().then((articles) => {
      this.articles = articles;
      console.log(articles);
    });*/

    this.articleService.getArticles().subscribe((articles) => {
      this.articles = articles;
      console.log(articles);
    });
  }

}
