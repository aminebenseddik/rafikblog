import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ArticlesPageComponent } from './pages/articles-page.component';
import { ArticleDetailPageComponent } from './pages/article-detail-page.component';
import { AdminPageComponent } from './pages/admin-page.component';
import { EditPageComponent } from './pages/edit-page.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'articles', pathMatch: 'full' },
    { path: 'articles', component: ArticlesPageComponent },
    { path: 'articles/:id', component: ArticleDetailPageComponent },
    { path: 'admin', component: AdminPageComponent },
    { path: 'edit', component: EditPageComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [ 
    ArticlesPageComponent,
    ArticleDetailPageComponent,
    AdminPageComponent,
    EditPageComponent
];