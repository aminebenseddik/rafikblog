export class Article {
  _id: string;
  title: string;
  subtitle: string;
  text: string;
  author: string;
  updated: string;
}
