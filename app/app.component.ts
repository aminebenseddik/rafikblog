import { Component } from '@angular/core';

@Component({
  selector: 'blogapp',
  templateUrl: './app/app.component.html',
})
export class AppComponent  {
  navBarLinks = [
    { text: 'Home', href: '/'},
    { text: 'Admin', href: '/admin' }
  ];
}
