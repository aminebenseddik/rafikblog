import { Injectable } from '@angular/core';
// import { ARTICLES } from './articles.data';
import { Article } from '../models/article';
import { Headers, Http } from '@angular/http';

import { Observable } from 'rxjs/Rx';
// import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';


@Injectable()
export class ArticleService {
  private apiUrl: string = 'http://localhost:4000/api/v1/articles';

  constructor(private http: Http) {}
  
  // Observables version
  getArticles(): Observable<Article[]> {
    return this.http.get(this.apiUrl)
            .map(response => response.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getArticle(id: string): Observable<Article> {
    return this.http.get(`${this.apiUrl}/${id}`)
            .map(response => response.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
