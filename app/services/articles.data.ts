import { Article } from '../models/article';

export const ARTICLES: Article[] = [
    {
      _id: '1',
      title: 'Man must explore, and this is exploration at its greatest',
      subtitle: 'Problems look mighty small from 150 miles up',
      text: '<p>Lorem Ipsum</p>',
      author: 'Amine',
      pubdate: '15 December 2016'
    },
    {
      _id: '2',
      title: "I believe every human has a finite number of heartbeats. I don't intend to waste any of mine.",
      subtitle: '',
      text: '',
      author: 'Bootstrap',
      pubdate: '15 December 2016'
    },
    {
      _id: '3',
      title: 'Failure is not an option',
      subtitle: 'Many say exploration is part of our destiny, but it’s actually our duty to future generations.',
      text: '',
      author: 'Amine',
      pubdate: '15 December 2016'
    },
    {
      _id: '4',
      title: 'Failure is not an option',
      subtitle: 'Many say exploration is part of our destiny, but it’s actually our duty to future generations.',
      text: '',
      author: 'Amine',
      pubdate: '15 December 2016'
    }
  ];
