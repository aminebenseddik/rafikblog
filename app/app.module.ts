import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent }  from './app.component';
import { NavbarComponent } from './components/navbar.component';
import { FooterComponent } from './components/footer.component';
import { HeaderComponent } from './components/header.component';
import { PageComponent } from './components/page.component';
import { ArticleBodyComponent } from './components/article-body.component';
import { ArticlePreviewComponent } from './components/article-preview.component';
import { TableRowComponent } from './components/table-row.component';

@NgModule({
  imports:      [
    BrowserModule,
    HttpModule,
    AppRoutingModule
    ],
  declarations: [
    AppComponent,
    routingComponents,
    NavbarComponent,
    FooterComponent,
    HeaderComponent,
    PageComponent,
    ArticlePreviewComponent,
    ArticleBodyComponent,
    TableRowComponent
    ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
