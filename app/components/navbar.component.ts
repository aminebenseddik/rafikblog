import { Component, Input } from '@angular/core';

@Component({
  selector: 'blogapp-navbar',
  templateUrl: './app/components/navbar.component.html'
})
export class NavbarComponent {
  @Input() navBarLinks: any[] = [];
}
