import { Component } from '@angular/core';

@Component({
  selector: 'blogapp-table-row',
  template: `
    <td>MyArticle</td>
    <td>Amine</td>
    <td>12 Decembre 2016</td>
    <td><a href="#">Edit</a></td>
    <td><a href="#">Publish</a></td>
  `
})
export class TableRowComponent {}
