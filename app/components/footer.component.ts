import { Component, Input } from '@angular/core';

@Component({
  selector: 'blogapp-footer',
  templateUrl: './app/components/footer.component.html'
})
export class FooterComponent {
  @Input() twitterLink: string;
  @Input() facebookLink: string;
  @Input() githubLink: string;
  @Input() blogTitle: string;
}
