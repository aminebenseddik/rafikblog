import { Component } from '@angular/core';

@Component({
  selector: 'blogapp-page',
  template: `
    <div class="container">
      <div class="row">
        <ng-content></ng-content>
      </div>
  </div>
  `
})
export class PageComponent {}