import { Component, Input } from '@angular/core';

@Component({
  selector: 'blogapp-article-body',
  template: `
    <div [innerHTML]="articleBody"></div>
    `
})
export class ArticleBodyComponent {
  @Input() articleBody: string;
}
