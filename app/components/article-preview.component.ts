import { Component, Input } from '@angular/core';
import { Article } from '../models/article';

@Component({
  selector: 'blogapp-article-preview',
  templateUrl: './app/components/article-preview.component.html'
})
export class ArticlePreviewComponent {
  @Input() article: Article;
}
