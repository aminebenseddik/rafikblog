import { Component, Input } from '@angular/core';

@Component({
  selector: 'blogapp-header',
  templateUrl: './app/components/header.component.html'
})
export class HeaderComponent {
    @Input() blogTitle: string;
    @Input() blogTagline: string;
    @Input() headerImage: string;
    @Input() author: string;
    @Input() pubdate: string;
}
